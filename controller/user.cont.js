const {User}= require('../models')
const Token = require('../utils')

class UserController {
    static async ShowAllUsers(req, res, next){
        try {
            // const userLogin = req.headers.userLogin
            // console.log(userLogin, "===========================isi userLogin")
            const users = await User.findAll()
            res.status(200).json(users)
            // res.render('Users')
        } catch (error) {
            console.log(error)
        }
    }


    static async register(req, res, next) {
        try {
            const { name , password , role } = req.body
            const payload = { name , password , role }

            if(password.length < 8) {
                res.status(400).json(
                    {
                        msg : "Periksa kembali data login anda (pastikan password lebih dari 8 karakter)"
                    }
                )}
            // else if (payload == null || payload == undefined || payload == ""){
            //     res.status(401).json({
            //         msg : "Data tidak boleh kosong"
            //     })
            //     }
            else{
                const newUser = await User.create(payload)
                console.log(newUser, "===REGISTERED USER===")
                res.status(200).json({
                    msg : "Registrasi berhasil, silakan log in"
                }
                )
                // res.redirect('/view/login')
            }

        } catch (error) {
            res.status(401).json({
                msg : "Terjadi kesalahan"
            })
        }
    }

    static async login(req,res,next) {
        try {
            const { name , password } = req.body
            const userLogin = await User.findOne(
            {
                where: {
                    name
                }
            })
        
        const _UserId = userLogin.dataValues.id
        const _password = userLogin.password
        if(password != _password) {
            res.status(401).json(
                {
                    msg : "data yang kamu input salah"
                }
            )}
            else {
                const _role = userLogin.dataValues.role
                
                const token = Token.generateToken({ name, role : _role , id : _UserId})
                console.log(token, "=============cek token di controller")
                if (token) {
                    req.header = token
                    res.status(200).json({ token })
                    // res.redirect('/view/users')
                }
            }
        }
        catch (error) {
        console.log(error)
            }
        }
    }

module.exports = UserController