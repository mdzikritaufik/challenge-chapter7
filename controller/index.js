const UserController = require('./user.cont')
const RoomController = require('./room.cont')


module.exports = {
    UserController,
    RoomController
}