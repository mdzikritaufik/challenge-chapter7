// const { json } = require("sequelize/types")
const { Room, UserRoom, GameHistory } = require("../models")
const Token = require("../utils")

class RoomController {
    static async createRoom (req,res,next) {
        try {
            const {RoomName}  = req.body
            const roomCreated = await Room.create({RoomName})
            console.log(roomCreated, "===================cek room")

                const idRoom = roomCreated.dataValues.id
                req.header = Token
                res.status(200).json(
                    {
                        msg : `Room berhasil dibuat dengan ID ${idRoom}`
                    }
                )
        
        } catch (error) {
            console.log(error)
        }

    }

    static async registerRoom (req,res,next) {
        try {
            const { id } = req.headers.userLogin
            const room_id = req.params.id
            const input = req.body.input

            await UserRoom.create({ UserId : id, RoomId : room_id })
            const room = await GameHistory.findOne(
                {
                    where: {
                        RoomId : room_id
                    }
                }
            )
            
            const roomHistory = room
            // const player2 = input
                
            if (roomHistory) {
                const player1 = room.dataValues.player1Picked
                
                
                if (player1) {
                    // const player2 = room.dataValues.player2Picked

                    const player2 = {player2Picked : input}
                    
                    await GameHistory.update(player2, 
                        {where :
                            {
                                RoomId : room_id
                            }
                        }
                    )
                }
            
                function rule (player1, player2){
                    if ( player1 == player2) return 0
                
                    if ( player1 == 'paper') return ( player2 == 'rock') ? 1 : 2
                
                    if ( player1 == 'scissors' ) return ( player2 == 'paper') ? 1 : 2
                
                    if ( player1 == 'rock' ) return ( player2 == 'scissor' ) ? 1 : 2
                }
                
                const player2 = room.dataValues.player2Picked
                const result = rule(player1, player2)
                console.log(result, "============pemenang")

                await GameHistory.update({winner : result}, 
                    {where :
                        {
                            RoomId : room_id
                        }
                    }
                )

                console.log(player1, "====player1")
                console.log(player2, "====player2")
            } else {
                await GameHistory.create(
                    {
                        RoomId: room_id,
                        player1Picked: input
                    }
                )
            }
            
            res.status(200).json(
                {
                    msg: `Selamat datang di room ${room_id}`
                }
            )
        } catch (error) {
            console.log(error)
        }
        
    }
}

module.exports = RoomController